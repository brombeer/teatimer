# Changelog

The format of this changelog is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Press <kbd>p</kbd> to pause/unpause the timer

## [1.0.0] - 2022-02-17

- Press and hold <kbd>1</kbd> to <kbd>0</kbd> to set the timer and save as default timer for application runs.
 f.e. pressing and holding <kbd>6</kbd> will set the timer **and** the default time of the app to 6 minutes. On the next app starts, teatimer will show 6 minutes

## [0.7.0] - 2022-01-16

### Added

- Press <kbd>v</kbd> to show version info
- Add this changelog

### Changed

- New dark boot splash
- New application icon
- Shortened keyboard number shortcuts in README
- Add attribution to README

## [0.6.0] - 2022-01-13

### Added

- Press <kbd>q</kbd> (in addition to <kbd>esc</kbd>) to quit the application
