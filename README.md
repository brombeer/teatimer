# teaTimer

A simple (to use), feature-packed tea timer.

## Usage

Press <kbd>1</kbd> to <kbd>0</kbd> to set the timer to a specific time

Press _and hold_ <kbd>1</kbd> to <kbd>0</kbd> to set the timer to a specific time and set the default timer (saves settings)

Press <kbd>esc</kbd> or <kbd>q</kbd> to quit the app

Press <kbd>m</kbd> to minimize the app window

Press <kbd>v</kbd> to show the app version

## Authors and acknowledgment

teaTimer app, pages, docs - brombeer

## Attribution

- "Godot Engine" - <https://godotengine.org/>
- "Hot Tea" icon - Pictograms by TinkTank.club - <https://icons.tinktank.club/>
- Font Digital Dream by Pizzadude - <https://pizzadude.dk>
- "bing" sound: BellRing_04.wav by Department64 - <https://freesound.org/s/550242/> [This work is licensed under the Attribution License.](https://creativecommons.org/licenses/by/3.0/#)

## License

MIT licensed.

## ChangeLog

All changes can be found in [changelog](https://gitlab.com/brombeer/teatimer/-/blob/main/changelog.md).
