extends Node

signal config_variable_set(key, value)
signal config_file_saved
signal config_file_loaded

var variables = {
	"default_timer": 180
}
var file = "user://teatimer.cfg"
var identifier = "teatimer"
var config = ConfigFile.new()


# Load config
func load():
	var err = config.load(file)
	# If the file didn't load, save the default config
	if err != OK:
		save()
	var default_timer = self.get("default_timer")
	variables["default_timer"] = default_timer
	emit_signal("config_file_loaded")


# Save settings
func save():
	self.set("default_timer", variables["default_timer"])
	config.save(file)
	emit_signal("config_file_saved")


func set(key : String, value):
	config.set_value(identifier, key, value)
	emit_signal("config_variable_set", key, value)


func get(key : String):
	return config.get_value(identifier, key)
