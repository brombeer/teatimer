extends Node

signal exit_app
signal timer_set(multiplier)
signal timer_hold(multiplier)
signal minimize_app
signal show_version
signal key_pressed(multiplier)
signal key_released(multiplier)

onready var key_hold_duration_threshold = 500
var time_key_press = 0
var time_key_release = 0


func _ready():
	InputHandler.connect("key_pressed", self, "_on_Key_press")
	InputHandler.connect("key_released", self, "_on_Key_release")


func _on_Key_press(multiplier):
	time_key_press = OS.get_system_time_msecs()


func _on_Key_release(multiplier):
	time_key_release = OS.get_system_time_msecs()
	var key_hold_duration = time_key_release - time_key_press
	if(key_hold_duration > key_hold_duration_threshold):
		emit_signal("timer_hold", multiplier)
	else:
		emit_signal("timer_set", multiplier)

func _input(event):
	if event.is_action_pressed("key_exit"):
		emit_signal("exit_app")
	if event.is_action_pressed("minimize"):
		emit_signal("minimize_app")
	if event.is_action_pressed("show_version"):
		emit_signal("show_version")

	if event.is_action_pressed("timerset1"):
		emit_signal("key_pressed", 1)
	if event.is_action_pressed("timerset2"):
		emit_signal("key_pressed", 2)
	if event.is_action_pressed("timerset3"):
		emit_signal("key_pressed",  3)
	if event.is_action_pressed("timerset4"):
		emit_signal("key_pressed", 4)
	if event.is_action_pressed("timerset5"):
		emit_signal("key_pressed", 5)
	if event.is_action_pressed("timerset6"):
		emit_signal("key_pressed", 6)
	if event.is_action_pressed("timerset7"):
		emit_signal("key_pressed", 7)
	if event.is_action_pressed("timerset8"):
		emit_signal("key_pressed", 8)
	if event.is_action_pressed("timerset9"):
		emit_signal("key_pressed", 9)
	if event.is_action_pressed("timerset0"):
		emit_signal("key_pressed", 10)

	if event.is_action_released("timerset1"):
		emit_signal("key_released", 1)
	if event.is_action_released("timerset2"):
		emit_signal("key_released", 2)
	if event.is_action_released("timerset3"):
		emit_signal("key_released",  3)
	if event.is_action_released("timerset4"):
		emit_signal("key_released", 4)
	if event.is_action_released("timerset5"):
		emit_signal("key_released", 5)
	if event.is_action_released("timerset6"):
		emit_signal("key_released", 6)
	if event.is_action_released("timerset7"):
		emit_signal("key_released", 7)
	if event.is_action_released("timerset8"):
		emit_signal("key_released", 8)
	if event.is_action_released("timerset9"):
		emit_signal("key_released", 9)
	if event.is_action_released("timerset0"):
		emit_signal("key_released", 10)

