extends Node2D

onready var timer_canvas = $TimerCanvas

var version = "1.0.0"
var timer_exit_app = 10 # Seconds to shutdown
var current_timer = 0
var is_running = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Config.connect("config_file_loaded", self, "_on_Config_loaded")
	Config.connect("config_file_saved", self, "_on_Config_saved")
	Config.connect("config_variable_set", self, "_on_Config_variable_set")
	InputHandler.connect("exit_app", self, "_on_Exit_app")
	InputHandler.connect("minimize_app", self, "_on_Minimize_app")
	InputHandler.connect("show_version", self, "_on_Show_version")
	InputHandler.connect("timer_set", self, "_on_Set_timer")
	InputHandler.connect("timer_hold", self, "_on_Hold_timer")

	Config.load()
	is_running = true
	current_timer = Config.get("default_timer")
	runTimer()
	set_process(true)


func _on_Timer_timeout():
	runTimer()


func _on_Config_loaded():
	timer_canvas.set_status_message("Settings loaded")


func _on_Config_saved():
	timer_canvas.set_status_message("Settings saved")


func _on_Config_variable_set(key, value):
	timer_canvas.set_status_message("Setting %02s changed to %02s" % [key, value])


func _on_Exit_app():
	ExitApp()


func _on_Minimize_app():
	timer_canvas.set_status_message("Minimizing...")
	OS.set_window_minimized(true)


func _on_Show_version():
	timer_canvas.set_status_message("Running version %s" % version)


func _on_Hold_timer(multiplier):
	setCurrentTimer(multiplier)
	Config.variables["default_timer"] = current_timer
	Config.save()


func _on_Set_timer(multiplier):
	setCurrentTimer(multiplier)
	timer_canvas.set_status_message("Setting timer to " + formatTime(current_timer))


func _on_ExitAppTimer_timeout():
	ExitApp()


func setCurrentTimer(multiplier):
	var duration = 60 * multiplier
	current_timer = duration

func runTimer():
	timer_canvas.set_timer(formatTime(current_timer))
	if (current_timer == 0):
		PlayAlarm()
		is_running = false
		set_process(false)
	if(is_running):
		current_timer = current_timer - 1


func formatTime(ticks):
	var time
	var minutes
	var seconds
	minutes = ticks / 60
	seconds =  ticks % 60
	time = "%02d:%02d" % [minutes, seconds]
	return str(time)


func PlayAlarm():
	timer_canvas.set_status_message("Playing alarm")
	if(is_running):
		get_node("AudioStreamPlayer").play()
		get_node("ExitAppTimer").start(timer_exit_app)


func ExitApp():
	timer_canvas.set_status_message("Exiting app")
	get_tree().quit()
