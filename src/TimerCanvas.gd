extends Node

# Set a dissapearing status message
func set_status_message(statusmessage):
	# Reset animation to start
	$AnimationPlayer.stop(true)
	var msg = str(statusmessage)
	$Canvas/StatusLabel.text = msg
	$AnimationPlayer.play("FadeOutStatusMessage")


func clear_status_message():
	$Canvas/StatusLabel.text = ""


func set_timer(message):
	$Canvas/CenterContainer/TimerLabel.text = String(message)
